# A hacky shell script to hopefully catch common errors.
# Just because the script flags something doesn't mean there's a mistake, and just because the script doesn't flag something doesn't mean there's no mistake etc.

echo "*** Checking for '. '"
grep -rF ". " classes/*.tex *.tex

echo "*** Checking capitalization."
grep -rP "(moveheading|movename|monster|monstername|healthmove)\*?[\n ]*{[^{}]*(?!I |I'm )[A-Z]" classes/*.tex *.tex
grep -rE "section\*?[\n ]*{[^{}]+[A-Z]" classes/*.tex *.tex
grep -rE "[aA]dvanced Move" classes/*.tex *.tex
grep -rE "[bB]asic Move" classes/*.tex *.tex
grep -rE "[cC]ommon Move" classes/*.tex *.tex
grep -rE "[sS]tarting Move" classes/*.tex *.tex
grep -rE "[hH]ealth Move" classes/*.tex *.tex
grep -rE "[hH]ealth Status" classes/*.tex *.tex
grep -rE "on(hit|partial|miss) *{[^a-zA-Z\\]*[A-Z]" classes/*.tex *.tex

echo "*** Checking for use of movename in move tags"
grep -rP "moveheading.+(requires|replaces|comeswith|removes|goesabove){((?!movename|emet).)+}"

echo "*** Checking for double ,, in triggers"
grep -rE 'trigger(\[If\])?{[^{}]+(movename{[^{}]+})?[^{}]+},' classes/*.tex *.tex

echo "*** Checking for empty line before a move"
grep -rPzo '\n(?!\\takenfrom|\\adaptedfrom|\\inspiredby|%|\\apocrypha{)[^\n]+\n\\moveheading[^\n]*\n?' classes/*.tex *.tex

echo "*** Checking for spacing issues."
grep -rF '\Blow ' classes/*.tex *.tex
grep -rF '\Blows ' classes/*.tex *.tex
grep -rF '\toxic ' classes/*.tex *.tex
grep -rF '\Toxic ' classes/*.tex *.tex
grep -rF '\bullseye ' classes/*.tex *.tex
grep -rF '\Bullseye ' classes/*.tex *.tex
grep -rF '\foul ' classes/*.tex *.tex
grep -rF '\Foul ' classes/*.tex *.tex

echo "*** Checking spelling alternatives."
grep -rE "[gG]rey" classes/*.tex *.tex
grep -rE "[cC]olour" classes/*.tex *.tex

echo "*** Checking for choose without macro"
grep -rE "[cC]hoose ([0-9]|one|two|three|four|five)" classes/*.tex *.tex

echo "*** Checking for hold without macro"
grep -rE "[hH]old ([0-9]|one|two|three|four|five)" classes/*.tex *.tex

echo "*** Checking for blow without macro"
grep -rE "textit{[Bb]low}"
