\documentclass{article}
\usepackage[normalgeometry]{localstyle}
\newcommand{\thisgame}{\textit{In the Blue Hours}}
\newcommand{\Thisgame}{\textit{In the Blue Hours}}
\title{In the Blue Hours}
\date{}
\begin{document}
\maketitle
\section*{Differences from Dungeon World}
\Thisgame{} is a hack of Dungeon World.
If you are familiar with Dungeon World, things should be quite familiar.
This section provides a high-level overview of what has been changed from Dungeon World.
If you are not familiar with Dungeon World, you can skip this section entirely.
\subsection*{Build your own class}
In Dungeon World, you select a playbook from a number of options to be your class.
In \thisgame{}, however, you build your own class by picking two smaller modules and combining them.
This gives you more ways to play and replay the game.

Classes also choose between starting items, with each starting item designed to encourage a particular play-style or specialization.
\subsection*{Items}
In Dungeon World, items are mostly a name and possibly some tags about the item.
In \thisgame{}, items may now be associated with complete moves, giving them more mechanical weight.
Additionally, combat is now handled by the item being used, rather than by the one-size-fits-all move of \movename{hack and slash}.
\subsection*{Health and damage}
Many changes have been made to health and damage.
As mentioned previously, combat is now handled primarily by the use of item moves.
HP, damage dice, armor, and piercing have totally been removed in favor of a \textit{health status} system (see \textit{Health and combat}).


\section*{The game}
\subsection*{Moves}
Moves are the backbone of this game.
They make up your abilities, items, and health.

\subsubsection*{Anatomy of a move}
Moves generally have a name, a body of text, and some tags.

\subsubsection*{Names}

Names are exactly what they sound like.
They provide a way to reference a move.
The name of a move appears in bold at the top of the move before anything else.
There's not much special about them, other than that they should be unique.

All player facing moves will have a name, but some GM facing moves (particularly the moves given to monsters) will not.
When the GM makes a new player-facing move it should always be given a name.

\subsubsection*{Body}

The body of a move is its most important component, detailing what it actually does.
It can take several forms.

The simplest form a move can take is a simple description of a fact about characters or the world.
For example, a move might say
``Your skin is made of diamond scales.
You cannot be harmed by fire.''
These moves establish these facts as true.

However, the most common form is the \textit{trigger-reaction} format.
There is a trigger which describes some circumstance, and a reaction that happens when that circumstance is fulfilled.
Often the reaction will involve some sort of roll of the dice to be resolved, but not always.
For clarity, the trigger is bolded and will be preceded by ``when''.

Similarly, moves can consist of an imperative which tells you what to do or changes the mechanics of the game but without a discrete trigger.
For example: ``You draw power from the waters of the world.
So long as you are standing in naturally occurring water, take advantage ongoing.''
This move consists of a description plus a imperative to ``take advantage''.

Moves will often consist of multiple components.
For example, a move might start with a description and then have a trigger-reaction.
A move might have multiple triggers, each with a reaction.

\subsubsection*{Tags}

Tags provide extra information and categorize moves at a glance.
This information may sometimes be redundant or obvious from the text of the move, but sometimes it provides new information.

Here we explain all the tags that can be used on moves:
\begin{itemize}
\item General tags:
  \begin{itemize}
  \item \tagdesc{Requires}{%
    The indicated moves are needed for this move to function.
    You cannot take advanced moves that require moves that you don't have.
    You may end up in possession of items that require moves you don't have; however, you cannot use their effect without having the required moves.
    }
  \item \tagdesc{Removes}{%
    When you take this move, you no longer gain any effect from the indicated move.
    Additionally, you cannot take the move that has been removed.
    If an item removes a move, and you stop owning the item, then you gain the effect of the previously-removed move again.
    (More common than \textit{Removes} is \textit{Replaces}, detailed below.)
    }
  \item \tagdesc{Replaces}{%
    Both requires and removes the indicated move.
    Much more common than \textit{Removes}.
    }
  \item \tagdesc{Comes with}{%
    When you take a move that comes with another move, you automatically take that move as well.
    If you already have the move the other move, this has no effect.
  }
  \end{itemize}
\item Advanced move tags:
  \begin{itemize}
    \item \tagdesc{\multiple}{%
      Upon taking this move, follow its effects but do not mark it.
      When you level up again, you can take this move.
      Moves will not \textit{Remove} or \textit{Replace} it.
    }
  \end{itemize}
\item Item tags:
  \begin{itemize}
    \itemtags{1}
    \itemtags{2}
    \item \tagdesc{Domain}{%
      Specific to the \textit{Blessed} class.
    }
    \item \tagdesc{Nature}{%
      Specific to the \textit{Familiar} class.
    }
  \end{itemize}
\item Class-specific tags:
  \begin{itemize}
    \item \tagdesc{Oath}{%
      Specific to the \textit{Hearth Knight} class.
      }
    \item \tagdesc{Way}{%
      Specific to the \textit{Monk} class.
      }
%    \item \tagdesc{Conflict}{% Vessel isn't in Beta; commenting this out for now.
%      Specific to the \textit{Vessel} class.
%    }
    \item \tagdesc{Domain}{%
      Specific to the \textit{Blessed} class.
    }
    \item \tagdesc{Nature}{%
      You must have a familiar with the appropriate nature in order for you to take this move or item.
      Specific to the \textit{Familiar} class.
    }
  \end{itemize}
\end{itemize}

\subsubsection*{Hold}
Moves may instruct you to ``hold'' some number or ``spend hold''.
Hold is an abstract unit employed by a number of moves.
When a move instructs you to ``hold'' a number you should increase your hold for that move by that number.
When a move instructs you to ``spend hold'' then you should decrease your hold as described by the move.
Spending hold generally gives the player a desirable effect and you cannot spend hold if you have none.

Hold is specific to the move that generates it and you should track hold independently for each move that mentions it.
You cannot gain hold with one move and spend that hold with another.

There are a few exceptions to the above.
Moves that modify or require another move may mention hold to refer to the original move's hold.
However in these cases it will likely be clear that the moves mean the same thing.

For making your own moves you should generally obey the following three rules:
\begin{itemize}
  \item If two moves both create and spend hold, they always use different hold.
  \item If neither of two moves mention each other explicitly, they use different hold.
  \item If a move either creates hold or spend it but not both, then it uses the same hold as another move.
\end{itemize}
If these conditions are impossible to acheive you should create a new named unit for clarity.

\subsection*{Rolling}

Many moves in \thisgame{} involve rolling two six sided dice (2d6).
Generally, a move will instruct you to roll dice and then use the result to select one of three options:
\begin{itemize}
\item A \fullsuccess\ (or \hit\ for short)
\item A \partialsuccess\ (or \partial\ for short)
\item A \miss
\end{itemize}
When a move instructs you to \textit{roll}, roll the 2d6 and add the results.
Many moves will also add one of your stat modifiers to the result.
For example, \textit{roll +WIS} means to add your WIS modifier to the result.
Rarely, moves may have no modifiers at all; because it is so uncommon, they will note this explicitly.
Then, using this score, you can determine which option is selected.
\begin{itemize}
\item If the result is $\geq 10$, then it's a \hit.
\item If the result is in the range 7--9, then it's a \partial.
\item If the result is $\leq 6$, then it's a \miss.
\end{itemize}

From here, the move will always give instructions for what happens on a \hit\ and a \partial, and may also give instructions on what happens on a \miss.
If there is no explicit instructions for a \miss, the default is that the GM makes a move.
Some moves will say that, on \miss, something happens in addition to the GM making a move.

Additionally, each time you roll a \miss, you gain 1 XP.
See \textit{Experience and leveling} for more detail.

\subsubsection*{Advantage and disadvantage}

Advantage and disadvantage are an additional modifiers that are applied to rolls.
They are not given as a part of the move you are rolling for, but rather come from other sources:
for example, one move might give you \textit{advantage} to another move under certain circumstances.
When you roll on that second move, you do so with advantage.

Advantage increases your chance of a \hit\ and disadvantage increases your chance of a \miss.
They do this by adjusting the ranges for the outcome.
Advantage increases the range for \hits\ to include 9 as well, making \hits\ more likely.
Disadvantage increases the range for \misses\ to include 7, making \misses\ more likely.

Advantage does not stack with more advantage and disadvantage does not stack with more disadvantage.
Even if you have advantage on a particular roll from two different places, it's no different from having advantage only once.
(If you include 9 in the \hit\ range, there's no way to include it a second time, so the second advantage has no effect.)
The same is true of disadvantage.

However, you can have both advantage and disadvantage at the same time on the same roll.
In that case, you have two competing forces pulling on the outcome of the result.
The result is likely to be extreme either way, so both the \hit\ and the \miss\ ranges are expanded.
Advantage and disadvantage don't ``cancel out'':
if you have advantage add 9 to the \hit\ range, and if you have disadvantage add 7 to the \miss\ range.

Here's a reference table so you can see the ranges for all the combinations:
\begin{center}
\begin{tabular}{|l|c|c|c|}
\hline       & \Miss & \Partial & \Hit \\ \hline
Neither      & 6-   & 7--9    & 10+ \\ \hline
Advantage    & 6-   & 7--8    &  9+ \\ \hline
Disadvantage & 7-   & 8--9    & 10+ \\ \hline
Both         & 7-   & 8       &  9+ \\ \hline
\end{tabular}
\end{center}

Moves will often give advantage/disadvantage ``forward'' or ``ongoing''.
Having advantage or disadvantage ``forward'' means that it modifies your next roll of that kind, and then goes away.
For example, if you have ``advantage forward to \movename{lob}'', then the next time you roll \movename{lob}, do it with advantage, but subsequent \movename{lob} rolls will be unaffected.
Having advantage or disadvantage ``ongoing'' means that you keep that modifier as long as the condition is met, no matter how many times you roll with it.
For example, if you have ``disadvantage ongoing to WIS rolls until you next sleep'', then any time you roll +WIS before the next time you go to sleep, take disadvantage.

You might accrue a couple of different advantages and disadvantages to various rolls with various conditions,
so it's a good idea to keep track of these somehow.
There should be space on your character sheet to write these down.

\subsubsection*{Summary}

Rolling is quite simple in \thisgame{} and you will quickly get the hang of it.
But to make it easier, here's a short step by step for the process:
\begin{itemize}
\item Roll 2 six sided dice.
\item Add the results.
\item Add any stat modifier given by the move to the result.
\item Determine if there is any advantage or disadvantage on the roll.
\item Determine which range the result falls under:
  \hit, \partial, or \miss
\item Follow the instructions for that result from the move.
\end{itemize}

\label{sec:health}
\subsection*{Health and combat}

Health in \thisgame{} works differently than many of the games it draws inspiration from.
Rather than having a numerical indicator of how much remaining health you have, there is instead a set of discrete states that your health can take, each with its own rules and consequences.
These states are called \textit{health statuses} and they are governed by their \textit{health moves}.
On the common moves sheet you will find the 5 common levels of health.
These are:
\begin{itemize}
  \setlength\itemsep{0.5mm}
  \item \movename{unharmed}
  \item \movename{scratched}
  \item \movename{battered}
  \item \movename{injured}
  \item \movename{struck down}
\end{itemize}
Each \textit{health move} tells you which other move it \textit{Goes above} along with how it is healed, and what happens when you receive the status.
Some class moves will introduce additional \textit{health statuses} beyond these.

Damage is not dealt with at a high level of granularity.
Rather than tallying up modifiers and bonuses,
all successful attacks boil down to an abstract unit of damage called a \Blow.

We define a \Blow\ to be enough harm to cause a change in a characters behavior, ability or disposition.
This means that it's relative to the situation and characters rather than some absolute.
For example, if you are walking through the forest when, out of nowhere, an arrow whips past grazing your arm, that's a \Blow.
Take \movename{scratched}.
However, if you are in an all out brawl with the mercenaries of the forgotten queen, and one of their arrows grazes your arm, that's nothing|you've already got worse wounds, don't adjust your status.

Importantly, an single attack or event never deals more than one \Blow.
This might seem a bit counterintuitive, since it would be expected that stronger attacks would hurt more.
But this exists to keep the pacing of fights fun, and it's still perfectly possible to have challenging fights this way.
You should care about your characters and it's just not fun when they get killed right out the gate because of some lucky combination of modifiers.
Instead, if your characters are going out, it should be in the dramatic climax of mortal combat.

That doesn't mean that all attacks are created equal.
Attacks can do things other than just a \Blow.
They can open up the player to dangers, make them vulnerable to other attacks, or strip them of any upper hand they might have gained.
Attacks that are more powerful or more dangerous should leverage these to give them weight rather than just beefing up the damage.

When a character takes a \Blow, it will trigger a \textit{health move}.
The basic \textit{health moves} for the player characters can be found on the move sheet.
Simply follow the move as you would any other.
When you take a \textit{health status}, it replaces whatever \textit{health status} you might have had previously.
Only the active \textit{health status} is triggered by a \Blow.
Moves also include a separate trigger for how to heal the status:
for example, if you are \movename{injured} and you receive medical attention and long rest, you do not become \movename{battered} or \movename{scratched}; rather, you are restored to \movename{unharmed}, i.e.\ fully healthy.

\subsubsection*{Non-player characters}
Non-player characters also have \textit{health statuses} and \textit{health moves}.
Mechanically, these work in the same way; however, each non-player character has their own track of \textit{health statuses}.
Often, they will have fewer statuses than players, and the statuses will behave in different ways.
While players' health moves are detailed to help players get a good sense of how things work, non-player health moves are more brief.
It is assumed that when they take a \Blow\ they drop down to the next level, so this is usually omitted from the move.

Some classes can animate creatures under their control.
In these cases, the class should include specific rules for the health moves of the animated creatures.
All other non-player characters should have \textit{health statuses} and \textit{health moves} determined by the GM.
These should not be visible to the players.
Specific instructions for making these for monsters can be found in the Monsters document.

\subsubsection*{Injury and debility}

Through the course of combat, players may receive injuries and debilities.
You receive a minor injury when you become \movename{battered} and a major injury when you become \movename{injured}.
A minor injury will heal, leaving a scar at the worst, while major injuries leave debilities when they heal.

Debilities are the lasting permanent effects of combat.
A debility should provide an obstacle for the player.
This can be through a mechanical change (for example ``a deep scar makes it painful to exert yourself; -1 STR''), or through something purely narrative (such as ``the physical wounds have healed, but you are left with an irrational fear of snakes'').

Debilities can be physical or psychological, but we recommend to stick mostly to physical.
We recommend also to be careful with decreasing the players' stats, since there are very few ways to increase stats once decreased.

Here are a few more examples of debilities:

\begin{itemize}
\item Your hand had to be amputated; -1 hand
\item You awoke in the hospital with a massive headache and a fuzzy recollection, -1 INT
\item Your face bears the telltale scars of Nsaa\~gi, the scraper.
  Everyone will know you he defeated you, but also that you survived.
\item The arrow spared your brain but not your eye.
  The part is crushed.
\end{itemize}

\subsubsection*{Comfort levels and boundaries}

It's important as a GM to strike a balance of filling the characters' lives with high stakes and tension without making their players uncomfortable.
For this reason, it is imperative to have a conversation as a group prior to starting about what kinds of injuries players are okay with.
See the \textit{Comfort levels and boundaries} section under \textit{Set up} for more information.

\subsubsection*{Allies, foes, and player vs.\ player combat}

Moves may use the terms ``ally'' and ``foe''.
These terms exist to imply what sort of character you want to target;
however, you are not bound by that implication.
An ally or foe can be any character regardless of their actual relationship to the players.
You can heal an enemy combatant, and you can curse another player character.

However, this game is not designed to handle player vs.\ player combat.
The combat is designed to be fun and well-paced for players fighting monsters.
It is not balanced to handle player vs.\ player combat.

Short combat between player characters can happen within the scope of roleplay.
However, it should only function as theatrics, not as a way of resolving conflict.
If a player attacks another player, the GM needs to stop the game and ensure that everyone at the table is happy with the combat in the story, and moreover that nobody is upset with anyone else at the table in real life.
As a litmus test the GM should try to find one specific result that everyone at the table is satisfied.
If everyone is easily pleased with a result, then you can either skip the combat and jump to that result, or you can choose to play it out.
If there is disagreement on what should come out of the combat, though, then you absolutely should stop there.

When players are upset with each other that needs to be settled outside of the game, and this book cannot help you with that.

Of course, these rules do not apply when an effect that would normally be negative is applied in a way that it is positive.
For example, a character who is a \textit{Monk} may ask another player to deal them a \Blow\ to trigger \movename{adrenaline rush}.
Doing so is not conflict, even though a \Blow\ is normally a negative effect.

\subsection*{Experience and leveling}

Over the course of your adventure, the heroes are likely to become stronger and gain new abilities.
Each time you fail a roll, unless otherwise specified, you gain 1 experience point (XP).
This is to encourage players to take risks during gameplay, and it is the main source of experience.
Other sources of experience, such as milestones, may exist as well.

Characters start at level 0.
When you have enough experience, you can spend it to become stronger, as described in the basic move \movename{level up}.
Unless otherwise specified, leveling up consists of gaining access to one advanced move of your choice.
This move can be from any class your character has.

Some groups may allow player characters to obtain additional classes beyond the two they start with.
This should be discussed before anyone tries to take a third class.
When playing with this optional rule, a player would take an additional class instead of choosing an advanced move as they level up,
and only if they have met the trigger condition for that class during gameplay (not in their backstory).

\section*{Set up}
To play \thisgame{}, you will need 3--6 players.
Having at least 4 is preferable; having 7 or more is possible but unwieldy.
One person will be the \textit{Game Master} (GM), and everyone else will be player characters (PCs).
It is the GM's job to be familiar with the rules and facilitate the game by describing the world.
It is the each player's job to decide what their characters say, think, and do.

To play this game, the GM will need:
\begin{itemize}
  \item A copy of these rules for reference.
  \item Paper for making maps and taking notes.
  \item Something to write with.
\end{itemize}
Each other player will need:
\begin{itemize}
  \item A pair of two six-sided dice.
    If you don't have that many dice, players can share dice.
  \item A printed copy of each of their class sheets.
    You can print these out in advance or wait until everyone has picked their classes.
  \item A printed copy of the character sheet.
  \item Something to write with.
\end{itemize}

You can play this game as a single game played in one sitting, or as a sprawling adventure played in several sessions across months or even years.
You should plan out the intended scope of your game before you begin, although a shorter game may end up growing into something longer.

\subsection*{Introductory discussion}

\subsubsection*{Comfort levels and boundaries}
First and foremost, it is important to ensure that everyone is having fun.
Emphasize that if anyone feels uncomfortable during the world-building process or while playing the game itself,
they should speak up and the table can come to a consensus that everyone is happy with.
If there are themes or elements that you as a player wish to avoid, which you think are at risk of coming up, now is a good time to mention this.
Likewise, if there are themes or elements that you as a player (or as the GM) wish to include,
but that run the risk of making others at the table uncomfortable,
you should mention it now so that you can have a conversation about how to handle it (even if the answer ends up being that it is not included at all).
There are many existing high-quality resources for having this kind of conversation, which can supplement the general guidelines here.

This conversation can be brief if you are playing with an established group.
However, you should still have it; don't assume that because you played one way last time, everyone wants to play it the same way this time.
When playing with people you don't know or a new group, it is imperative to have a thorough conversation.

It is not the GM's job to choose a solution or break ties,
nor is it a vote where one faction out-numbers another;
it is on the group to decide something that suits everyone.
If that is unworkable, it may be better to find a different game to play or a different group to play with.

One topic that is especially important to discuss is injuries to the player characters and other key figures in the narrative.
It is fine to play games about heroes, who bounce back unscathed from improbable circumstances.
It is fine to play a game with punishing combat that chews up its adventurers.
Most groups will find their spot in the space between those options; however, it is not a linear scale.
It is fine to play with some sorts of injury and debility but not others.
It's fine to want to stay away from gory descriptions but keep the mechanically punishing aspects, or to want only physical debilities but not mental ones.
It is also fine for injuries to different characters to be handled differently, based on their players' comfort levels, so long as everyone is okay with that.
If there is disagreement, we strongly recommend that you compromise on the side of softer combat, but everyone should still be happy with the result.

Storytelling is always a process involving both the players and the GM.
However, we note with specificity that a player always has the right to refuse any injury or debility that they are uncomfortable with their character receiving.
In this case, the GM should offer them a different injury or debility in its place.
If this becomes a common occurrence, then we recommend that you re-initiate the conversation about the role of injury in the game.
It is very reasonable for a player to initially think that something would be fun, but to find out later that they do not find it fun.
It is okay to change how things are handled partway through.

\subsubsection*{World-building questions}
At this point you should have decided who is going to be the GM and who will play characters, as well as having had a conversation about the type of game you want to play.
Before you get into creating characters, you are going to answer some questions about the world you will play in to set the tone and start the world-building process.

The GM should be the one asking the questions.
However, \textit{everyone} at the table, including the GM, should be included in answering them.

\noindent Start by reading:

\vspace{4mm}
\begin{minipage}{\dimexpr\textwidth-3cm}
  \noindent\textit{We live in interesting times.}
  \begin{itemize}
  \item \textit{What great power has lost, is losing, or is about to lose its grip or influence on society?}
  \item \textit{What new or reemerging power is apparent on the horizon to replace it?}
  \end{itemize}
\end{minipage}

\vspace{6mm}
As GM, you have explicit permission to modify or replace these questions as you see fit.
However, we recommend that for at least your first game as a GM you use the questions as-is, because it is easier to make informed changes once you have more experience with the game.

Once the questions have been answered, the GM should summarize the results, and then you can move onto the next section.

\subsection*{The characters}
The next thing you will want to do is to create each player's character.
We will be creating not just the stats and abilities for each character, but also their personalities, their goals, and their past.
This works best when you start character creation with some ideas of what sort of character you are interested in playing, but a flexible attitude.
This means being open to building off of other players' ideas.
You should not come into this expecting to recite prepared answers to the questions.

The GM will call upon each player, in order, to select a class.
Each class has a trigger.
When you select a new class, explain how your character met the trigger condition.
It's okay if your story doesn't line up with the provided trigger, as long as everyone is happy with it.
Since you are establishing the characters and the world, your answer should be at least a few sentences and can introduce characters and locations not previously mentioned.
If you are unsure or need a good idea, feel free to ask the table for ideas.
A class may require you to make some choices in its \textbf{Basic moves} section.
Make these choices and explain the answers to the group.
The GM or other players may also ask you to expand on your character's backstory with a few questions before moving onto the next player.

Go around the circle twice so that each player has selected two classes.
If you want to change your answers to your earlier questions, you may do so.
Nothing is set in stone at this point; you are just getting an idea for your characters and their relationships.

At this point each character should fill in their 6 stats.
Assign one of each stat to the values -1, +0, +0, +1, +1 and +2.
You can do this part independently.

Once you have chosen your classes and assigned your stats, each player should give a short introduction.
This should include what your character will be named or called, along with what information you think is most relevant to introducing your character.
Most likely everyone at the table already knows your character a bit, but that's okay.
Give your introduction anyway as if they were meeting your character for the first time.

\subsubsection*{Choosing a class}

The current list of classes in \thisgame{} is as follows:

\begin{itemize}
\item \textbf{Animator}: Create golems out of raw materials to assist you in various ways.
\item \textbf{Blessed}: Perform acts of piety to build favor with your deity, and use your deity's favor to seek guidance and healing.
\item \textbf{Cook}: Collect ingredients to make magical food that strengthens you and your friends.
\item \textbf{Hearth Knight}: Swear oaths that limit you, in exchange for overwhelming power.
\item \textbf{Herbalist}: Use your botanical knowledge to achieve unprecedented healing.
\item \textbf{Hunter}: Apply the skills that you've developed from living off the land to track down and defeat anything.
\item \textbf{Meteoromancer}: Control the weather and gain corresponding abilities from it.
\item \textbf{Monk}: Use your discipline and resolve to overpower stronger or better-armed foes.
\item \textbf{Thief}: Use deception and guile to achieve your goals, relying on social graces rather than stealth.
\item \textbf{Poisoner}: Incapacitate your enemies with a variety of poisons.
\item \textbf{Witch}: Work together with your strange otherworldly companion to do magic.
\apocrypha {
	\item \textbf{Half-Mage}: Build your own class from parts of others, as a jack-of-all-trades, master of none.
}
\end{itemize}

It's fine if two characters have the same class.
But in that case, they shouldn't share their other class, and they should pick different options wherever possible for options such as class items.
Some classes have more customization options than others, so if you're going to have two people share a class, it's best if it's one of the more customizable ones.

\subsection*{The world}


\subsection*{The gear}

Now your characters need some items.
The GM should once again call on each player in order.
When called on, the player should select one item from the item list of their first class.
Add this item to your inventory and explain how it came to be in your possession.
Was it a gift from some noble benefactor?
Did you spend months in the forge shaping it from raw materials?
Did you steal it?

Once every player has gone, go around a second time and do the same for your second class.

Now you will go around one final time.
This time each player may purchase two items, each equal in value to a \textit{shiny} item, and add them to their inventory.
No explanation is needed for this part, but you are always welcome to elaborate on aspects of your backstory.

\subsection*{The party}

At this point, your characters are all complete individually, but there's nothing tying them together.
Take a moment to discuss how the characters know each other.
Your character should have at least met everyone else's character, and you should have a closer relationship with at least one other character.

Once you have made your characters and decided on their gear, you're ready to begin playing the game.
Decide on a shared goal for the player characters to start with.
It is likely that the characters' backstories and the world you have built suggest an obvious answer here,
but it can never hurt to have things spelled out explicitly.
In the same way, agree on a first step towards accomplishing this goal, and then set off on your adventure.

\section*{GMing}

% Put some opening nonsense here.

\takenfrom{Dungeon World}
\subsection*{How to GM}
\trigger{you sit down at the table to GM} you do these things:
\vspace{-0.5em}
\begin{itemize}
	\item Describe the situation
	\item Follow the rules
	\item Make moves
	\item Exploit your prep
\end{itemize}
The players have it easy---they just say what their characters say, think, and do.
You have it a bit harder.
You have to say everything else.
What does that entail?

First and foremost, you \textbf{describe the immediate situation around the player at all times}.

\adaptedfrom{Dungeon World}
\subsection*{Agenda and principles}

As a GM you have an \textit{Agenda} which gives you overarching goals to guide the game, and \textit{Prinicples} which are narrower directives to push you towards your agenda items.

\vspace{1em}
\noindent Your agenda items are:

\subsubsection*{Portray a living world}

\begin{itemize}
  \item \textbf{Address the characters, not the players.}
    It's important to keep players immersed in the world you are portraying, so use their names!
    When you address the players it draws people away from your world, but when you address the characters it helps to make things feel real.
  \item \textbf{Give every creature life.}
    Creatures in living worlds are not just mindless monsters to be slain or obstacles to overcome.
    They have wants, desires, and reasons for doing the things they do.
    Give creatures depth, but don't cry when they get beat up or overthrown.
  \item \textbf{Name every person.}
    Any person that the players speak with must have a name, whether the players know it or not.
    They probably have a personality and some goals or opinions too, but start with a name.
  \item \textbf{Think offscreen too.}
    A living world has things happening everywhere, not just where the players are.
    Sometimes your best move is somewhere else.

    If the players decided not to chase down that goblin, what is she doing now?
    If the players spent a week hunting around caves for golden eggs, how have the duke's armies advanced in the meantime?

    Make your move offscreen, but give the players a sign that something is unfolding.
    If they choose to ignore it, let the consequences follow.
  \item \textbf{Begin and end with the fiction.}
    Everything you and the players do is a reaction to something in the fiction and the cause of something else in the fiction.
    When the players make a move, they take a fictional action to trigger it, apply the rules, and get a fictional effect.
    When you make a move it always comes from the fiction.
\end{itemize}

\subsubsection*{Fill the characters' lives with adventure}

\begin{itemize}
  \item \textbf{Be a fan of the characters.}
    An exciting story needs great protagonists,
    so let your players be those great protagonists.
    Cheer for their victories and lament their defeats.
    You're not here to push them in any particular direction, merely to participate in fiction that features them and their action.
  \item \textbf{Think dangerous.}
    A great story needs tension.
    For the players to succeed there must be something to overcome;
    there must be risk of failure.

    No single life is worth everything and there is nothing sacrosanct.
    Everything can be put in danger, everything can be destroyed.
    Nothing you create is ever protected.
    Whenever your eye falls on something you've created, think how it can be put in danger, fall apart, or crumble.
    The world changes|and without the characters' intervention, it changes for the worse.
\end{itemize}

\subsubsection*{Play to find out}

\begin{itemize}
  \item \textbf{Make a move that follows.}
    When you make a move, choose the move that best fits the situation.
    Not the move that steers the game in a certain way, nor just a random move off the list.
    It is important to make the move that fits best with the game even if it takes a bit of time to think.
  \item \textbf{Never speak the name of your move.}
    Don't say which move you are making, just make it.
    Say the name of your move explicitly can ruin the flow of the game.
    Sometimes, when a move feels unnatural, a GM will want to say ``I'm just doing what the rules say'' or ``The rules say I can do this''.
    But as a GM you are responsible for justifying why you are making a move without referencing the rules.
    It needs to come from the situation.
  \item \textbf{Draw maps, but leave blanks.}
    Maps help everyone stay on the same page.
    When you describe a new location, use a map to help you.
    However, part of playing to find out means that your map is never complete.
    Leave blanks in your map that even you don't know the contents of, so that your players can explore.
  \item \textbf{Ask questions and use the answers.}
    Part of playing to find out what happens is explicitly not knowing everything, and being curious.
    If you don't know something, or you don't have an idea, ask the players and use what they say.

    The easiest question you will use is ``What do you do?''.
    Whenever you make a move, end with ``What do you do?''.
    You don't even have to ask the person you made the move against.
    You can take that chance to shift the focus elsewhere:
    ``Rath's spell is torn apart with a flick of the mage's wand.
      Finnegan, that spell was aiding you.
      What are you doing now that it's gone?''
  \item \textbf{Allow multiple approaches.}
    In this game, you set up the pins and the players knock them down, but you don't get to say how.
    Every time you put an obstacle up for the players, you should be ready for at least two ways to approach it, each with their own strengths and drawbacks.
    Be ready for the players to surprise you with a third approach you hadn't even considered.
\end{itemize}

\adaptedfrom{Dungeon World}
\subsection*{Moves}

Just like a player, the GM has moves, but GM moves are a little simpler and more open ended.
You make a GM move when:
\begin{itemize}
  \item everyone at the table looks at you to see what happens next.
  \item the players give you a golden opportunity.
  \item a player rolls a \miss\ on one of their moves.
\end{itemize}
Your moves are not magic spells or part of the grammar of the game, and you never speak the name of your moves.
Instead, simply narrate what happens.

\vspace{1em}
\noindent Your moves are:
\begin{itemize}
	\item \textbf{Use a creature, danger, or location move.}
    Every creature comes with moves for you to use with it.
    Some locations have moves too.

	\item \textbf{Reveal an unwelcome truth}
	\item \textbf{Show signs of an approaching threat} 
	\item \textbf{Deal a \Blow}
	\item \textbf{Use up their resources}
	\item \textbf{Show them the consequences of their actions}
	\item \textbf{Give an opportunity that fits a class' abilities}
	\item \textbf{Show a downside to their class or equipment}
	\item \textbf{Offer an opportunity}
	\item \textbf{Offer an opportunity with cost}
	\item \textbf{Put someone in a spot}
	\item \textbf{Show them the obstacles and tell them the costs}
\end{itemize}


\end{document}
