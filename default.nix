{ pkgs ?
  import (builtins.fetchGit {
    name = "nixpkgs-unstable-2021-07-19";
    url = "https://github.com/nixos/nixpkgs/";
    ref = "refs/heads/nixpkgs-unstable";
    rev = "bfef28861df7a97533e9b9b04ef66019edb390fb";
  })
  {}
}:

pkgs.stdenv.mkDerivation rec {
  pname = "dungeon-compendium";
  version = "0.1";
  src = ./.;
  buildInputs =
    [ ( import ./latex.nix { pkgs = pkgs; } )
    ];
  buildPhase = ''
    export HOME=$TMP
    latexmk -pdf main.tex
    latexmk -pdf MoveSheet.tex
    latexmk -pdf Shop.tex
    latexmk -pdf Monsters.tex
    latexmk -pdf LostAndFound.tex
    latexmk -pdf CharacterSheet.tex
    latexmk -pdf classes/Animator.tex -outdir=classes/
    latexmk -pdf classes/Antiquarian.tex -outdir=classes/
    latexmk -pdf classes/Blessed.tex -outdir=classes/
    latexmk -pdf classes/Cataclyst.tex -outdir=classes/
    latexmk -pdf classes/Cook.tex -outdir=classes/
    latexmk -pdf classes/Entomologist.tex -outdir=classes/
    latexmk -pdf classes/HalfMage.tex -outdir=classes/
    latexmk -pdf classes/HearthKnight.tex -outdir=classes/
    latexmk -pdf classes/Herbalist.tex -outdir=classes/
    latexmk -pdf classes/Hunter.tex -outdir=classes/
    latexmk -pdf classes/Meteoromancer.tex -outdir=classes/
    latexmk -pdf classes/Monk.tex -outdir=classes/
    latexmk -pdf classes/Necromancer.tex -outdir=classes/
    latexmk -pdf classes/Poisoner.tex -outdir=classes/
    latexmk -pdf classes/Strider.tex -outdir=classes/
    latexmk -pdf classes/Telepath.tex -outdir=classes/
    latexmk -pdf classes/Thief.tex -outdir=classes/
    latexmk -pdf classes/Tinker.tex -outdir=classes/
    latexmk -pdf classes/Vessel.tex -outdir=classes/
    latexmk -pdf classes/Whisperer.tex -outdir=classes/
    latexmk -pdf classes/Witch.tex -outdir=classes/
  '';
  installPhase = ''
    mkdir $out
    install -t $out main.pdf MoveSheet.pdf Shop.pdf Monsters.pdf LostAndFound.pdf CharacterSheet.pdf
    mkdir $out/classes
    install -t $out/classes classes/*.pdf
  '';
}
