Based on the work of Sage LaTorra and Adam Koebel. This work is licensed under the Creative Commons Attribution 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

# Changes from DW
There are some important differences from DW to consider.
 * +1/-1 forward/ongoing is replaced with *advantage*/*disadvantage* forward/ongoing.
 * Health system is overhauled.
   No more damage dice.
 * No more Hack and Slash / Volley, weapons have their own moves for attacking.


# To add a compendium:
Add a tex file to the classes directory.
Include
```
\usepackage{localstyle}
```
to get all the formatting used in the other classes.
There is not yet a full documentation on how to use the macros so, look at the existing classes and mimic them.

I recommend to compile and read main.tex to get an idea of how classes are being used.
A class should aim to have, a solid trigger that prompts good story without constraining things too much.
A class should have 2-4 class items.
Class items should aim to add a unique spin on each class.
Items that mix up play style or change the way a class works are ideal.
Balance is a secondary concern.
It is also probably ideal that each class have access to a weapon as a starting item.
These weapons should probably be decently good weapons.

Some of the classes here might not currently be meeting these standards but that is the goal.

# Building

To ease the process of compiling all the tex files this project is set up so that it can be built using [Nix](https://nixos.org/).
If you have Nix installed, all you need to do to compile all the tex files to PDFs is to run the `nix-build -v` command from the root directory of the project (this may take some time the first time you build since Nix will need to download dependencies).

The project is pinned to a particular version of the Nix packages set.
If you wish to instead specify your own version of the Nix packages you can instead build with the following command:

```bash
nix-build -v --arg pkgs "<<Nix expression evaluating to the desired packages set>>"
```

If you are running nixos you can use the following command verbatim:

```bash
nix-build -v --arg pkgs "import <nixpkgs> {}"
```

This will likely build the project far faster than the regular command.

## Shell

If you only want to compile a couple files, for example if you are working on the project you can use a nix shell.
Run the following to start the shell:

```bash
nix-shell --pure
```

From there it should set you up with an appropriate latex environment and you can compile files with `pdflatex`.

The nix shell is not pinned to any version of the Nix packages set, so to ensure that everything compiles it is best to run a full build once you are done.

## Without Nix

Nix provides a stable environment for compiling latex.
However if you don't have Nix you can still try to compile the files with any standard latex compiler.
We however make no guarentees that your specific environment will work and if it doesn't our suggestion is to use Nix.

# Liscenses and attribution

The text of this game is distributed under the CC-BY-SA-3.0 and CC-BY-SA-4.0 liscenses.
The source code for the documents are distributed under the AGPL liscense.
All three liscenses can be found included with the source.

Portions of this game are adapted with permission from several sources.
* Dungeon world: This game is based on and borrows many concepts from the game *Dungeon World* by Sage LaTorra and Adam Kobel.
* Perilous wilds: This game borrows some content from *The Perilous Wilds* by Lampback & Brimstone.
* Lob: The *Lob* move is adapted from a post by stack exchange user Glazius.
* Parley: The *Parley* move is taken with permission from Jeremy Strandberg's blog *Spouting Lore*.
