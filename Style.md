# Style guide

This document outlines a unified style guide for this project.

## Prose

### Serial comma
Use a serial comma (also known as an "Oxford comma") before a conjunction (e.g. "and" or "or") in a sequence of three or more terms.
Do not use a comma for a sequence of two terms unless to separate a clause boundary.

### Title case
Follow ordinary sentence case for titles, including move titles.
This means capitalize the first character of the title, the first characters of proper nouns, and the word "I" along with its contractions (e.g. "I'm" and "I've"), but otherwise use lower-case.

### Key terms
Key terms are words given specific meaning in the context of the game.
They include the names of moves, the names of classes, move tags, and resources.

#### Italics
Always write a key term in italics to distinguish it from the ordinary usage of the word.
The exception being the six stats, which are a key terms, but should not be italicized.

#### Capitalization
The names of moves and classes appearing in prose should be always capitalized using title case.
For example

> Take 2 extra hold when using *Darker secrets*.

The `\movename` macro will do this automatically when used on moves.

With the exception of stats, other key terms, such as move tags, resources, and abstract units, should be written entirely in lower case.
As with italics, the six stats are once again a special case and should be written in all upper case.

#### Stats
Player stats should be written using their three letter abreviations in all upper case.
The six stats are:

- CHA (charisma)
- CON (constitution)
- DEX (dexterity)
- INT (intelligence)
- STR (strength)
- WIS (wisdom)

When instructing the player to roll with a particular stat, use the "+" character.
Place a space between "roll" and "+" but no space between "+" and the stat.
For example:

> Roll +DEX

Capitalize "roll" if and only if it appears at the beginning of a sentence.

### Moves
Moves are the basic building block of the game.
Moves should consist of a title and a body.

The body of a move for an item may start with a noun phrase describing the item and ending with a full stop, but otherwise the body of a move should be written in complete sentences.

#### Check boxes
Check boxes should be provided to the left of some move titles to help players track what moves they have taken.
In general, check boxes should only be placed next to the following:

- Class items.
- Advanced moves.
- Basic moves which the player can opt in or out of during character creation.
- Milestones. (done automatically by `\milestonemove`)

However special cases do exist, such as moves that are not technically advanced moves, but act like them, or moves that aren't class items, but act like them.

#### Triggers
The trigger of a move helps to tell players when a move should be used.
The trigger should be bolded and preceded by "when" or "if".
The "when" or "if" should not be bolded and should be capitalized appropriately.
Use "if" only when "when" would be inappropriate or awkward.
Prefer "when" for almost all cases.

### Gender neutrality
Use gender neutral language wherever possible and appropriate.

Use "they" (and its forms) as a third person singular gender neutral pronoun.
For example:
> When a player triggers a move, they must check if a roll is required.

### Whom
Use "whom" as the oblique relative pronoun.
For example:
> Take advantage against the foe whom the piece came from.

### Prescriptive latinizations
This project is written in English for English speakers.
There is no expectation to follow those "rules" of English imported from Latin, which have never been widely followed by English speakers.

For example it is fine to:
- "Split" infinitives.
- "Strand" a preposition.

### Quotations
Use so-called "logical quotation" style.
Only use punctuation inside of quotation marks it if it appears in the quoted text.

For example the following are valid:
> Are you sure they said "elves"?
> The guard says "Who are you?" to everyone that passes here.

### Numbers
Numbers should be spelled out except in the following cases:

- Dice results and modifiers on them
- Experience points
- Numbers larger than twenty

### Shorthand symbols
The shorthand symbols `#`, `&`, and `@` should be avoided.
`+` should not be used in place of "and" either.
Prefer to spell out the complete word.

### Spelling
Generally use accepted American English spelling, except where a non-standard spelling is inappropriate.

#### Individual words

The following are a list of words which would be ambiguous under the above rules along with the prescribed spelling for this project:

- "Gray", not "Grey".
- "Leveling", not "Levelling".
- "Okay", not "Ok".
- "World-building", not "Worldbuilding" or "World building".

## Code

### Macros
Use macros to handle formatting when the exist and are appropriate.
For example, the following macros should always be used when appropriate:

- `\trigger`
- `\onhit`
- `\onpartial`
- `\onmiss`
- `\moveheading`

### Indentation
Use two spaces per level of indentation.

### Linebreaks

#### Sentence end
Always split sentences onto separate lines following a full stop (".", "?", or "!").
You may also split particularly long sentences into multiple lines along other punctuation (e.g. "," or ";");
although it may often be better to break such sentences into multiple smaller sentences.

If multiple sentences are enclosed in curly braces, put each sentence (or fragment ending in a full stop) indented on its own line.
Place the opening and closing braces on the lines before and after the insides.
Use `%` to ensure the spacing in the compiled document is correct.
For example:

```latex
\onhit{%
  you find many mushrooms.
  Select three mushrooms from the mushroom table to add to your inventory.
}%
\onpartial{you find only one mushroom.}
```

When using a full stop character other than as a full stop, (e.g. in "e.g.") do not split the lines.
If the full stop character is followed by a space it is also important to note that it will be treated as a full stop by latex.
In these cases place a slash before the space to fix the issue.

```latex
Remove any clothing (e.g.\ hats or gloves) that were affected by the spell.
```

In general running

```shell
fgrep -r ". " *.tex
```
in the project should yield no results.

#### Titles
Place two linebreaks before each move title creating a blank line which visually separates moves.

When using any of the following macros:

- `\moveheading`
- `\movename`
- `\monster`
- `\monstername`

Only capitalize the first character of the sentence if it is a proper noun or a variation of the word "I".
The first letter of the sentence will be capitalized automatically to comply with the Manual of Style.
Keeping things lowercase helps make future refactors easier.

### Attribution
Use the attribution macros where appropriate.
Place them before on the line before the title for the relevant item.

The macros are:
- `\takenfrom`
- `\adaptedfrom`
- `\inspiredby`

### List sheets
For sheets that contain large lists of things (e.g. the monster list contains a large number of monsters and the shop contains a large number of game items) define all the items in a header using `\NewDocumentCommand` and invoke them in the body of the list.

The item definitions should be sorted alphabetically by command name.
The command name should be camel case.
The command name should be similar to the actual name of the thing being defined.
However, if the name is changed the command should not be changed.

If an item is deleted, the definition should remain and the invocation should be deleted.
This way if we choose to restore the item to the list at a future point then the git blame is maintained.

Attribution should be placed inside the definition of the item.

The idea is that items in large lists frequently need to be reorganized or adjusted for spacing.
Organizing items this way reduces the diffs created by such changes and makes these changes simpler to perform.

For example this diff shows two items being swapped.

```
--- a/Shop.tex
+++ b/Shop.tex
 \begin{minipage}[t]{.48\textwidth}
   \RoseFromTheDead
   \EyeOfTheGraySisters
-  \BazaarBag
   \FlaskOfBreath
+  \BazaarBag
   \OlmArm
 \end{minipage}
```

It's immediately clear from this that the content of the items was not altered.

### Special characters
Source code should contain avoid non-ASCII characters.
This is to make it as easy to edit without specialized keyboard setups.
If a non-ASCII character is needed it is best to first see if a macro can be used to type the character.

For example prefer `G\"odel` over `Gödel` since `ö` is not a ASCII character.

There are some LaTeX packages, such as the `tipa`, use specialized fonts to make ASCII characters look like non-ASCII characters in the rendered output.
Do not use this method.
This results in the text appearing correct but will be read incorrectly by screen readers or to copy-paste.
