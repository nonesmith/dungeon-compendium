{ pkgs ?
  import (builtins.fetchGit {
    name = "nixpkgs-unstable-2021-07-19";
    url = "https://github.com/nixos/nixpkgs/";
    ref = "refs/heads/nixpkgs-unstable";
    rev = "bfef28861df7a97533e9b9b04ef66019edb390fb";
  })
  {}
}:
  pkgs.mkShell
    { nativeBuildInputs =
      [ ( import ./latex.nix { pkgs = pkgs; } )
      ];
    }