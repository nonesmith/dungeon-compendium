<!-- Link to the source for the class. -->
[Go to source.](https://gitlab.com/nonesmith/dungeon-compendium/-/blob/master/classes/<!-- Class name here. -->.tex)


<!--
This label indicates it is a tracking issue for a class:

/label ~"issue-type::class"

Add other appropriate labels:

label ~"advanced-moves::none"
label ~"advanced-moves::needed"

label ~"items::none"
label ~"items::needed"

label ~"needs-other"

label ~idea
label ~"class-status::unplayable"
label ~"class-status::unfinished"
label ~"class-status::complete"
-->
