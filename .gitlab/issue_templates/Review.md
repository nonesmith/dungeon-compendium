<!-- Add the class name and its issue number. -->
# Narrative Review of Class (#N)
<!--
Each review item summarizes the thoughts on the item then gives a verdict as to its impact on the narrative.
Options are:
  **Low impact** : Has almost no impact on the narrative. e.g. gives advantage to some roll.
  **Medium-low impact** : Has some impact on the narrative.
  **Medium impact** : Has a middling amount of impact on the narrative.
  **Medium-high impact** : Has a lot of impact on the narrative. It almost always marks a significant beat or fork in the story.
  **High impact** : Has a lot of impact on the narrative. It introduces new side-quests NPCs or characters. Changes the story with lasting effects.
  **Potentially derailing** : Has so much impact on the narrative it harms the game. It makes the entire story center on one person for extended periods of time, or it creates complex sprawling side quests that interfere with main lines.
  **Not applicable** : Cannot be assessed for narrative impact.
-->

## Trigger
<!-- Review the trigger -->

## Basic Moves
<!-- Add each basic move below. -->
### Basic Move 1
<!-- Review Basic Move 1. -->

## Class Items
<!-- Add each class item below. -->
### Class Item 1
<!-- Review Class Item 1. -->

## Advanced Moves
<!-- Add each advanced move below. -->
### Advanced Move 1
<!-- Review Advanced Move 1 -->

## Result

<!-- Summarize the result and give the final verdict. -->
<!-- ~"review-status::low-impact" -->
<!-- ~"review-status::medium-impact" -->
<!-- ~"review-status::passed" -->

## Action items

<!-- List things to be done with the class based on the review. -->
<!-- When all items are checked off you can close the issue -->
- [ ] Fix *Basic Move 1*.

<!--
/label ~"issue-type::review"
-->
