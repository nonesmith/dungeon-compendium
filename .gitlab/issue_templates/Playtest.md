# Summary of playtest

## Players

<!-- Enumerate the player characters with their names classes, items and other relevant character info. -->
- "Bob": Artemancer + Scryer (Arboreal wand, Hydronomicon, Oath of War)

## Sessions

<!-- Briefly summarize all sessions -->
X sessions.
...

# Reflection

<!-- Things you noticed during the playtest -->

# Action items

<!-- Add fixes or tweaks that you noticed should be made during the playtest. -->
- [ ] Action item 1.

# Questions

<!-- List small questions that have come up here. -->
<!-- Big questions should go in separate issues. -->
- Question 1?

<!--
/label ~"issue-type::playtest-result"
-->

<!-- Close the issue when all action items have been done and all questions have been answered. -->
