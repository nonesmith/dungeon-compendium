{ pkgs }:
  pkgs.texlive.combine
    { inherit (pkgs.texlive) scheme-basic
      enumitem
      titlesec
      metafont
      ec
      fontsize
      # Needed for fontsize
      xkeyval
      latexmk
      pgf
      standalone;
    }